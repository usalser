/* usalser -- USALS motor rotator
 *
 * Created by Ondrej Caletka <o.caletka@sh.cvut.cz>
 * Created from femon by Johannes Stezenbach <js@convergence.de>
 * Modified by Jiri Jansky <janskyj@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* gcc -o usalser usalser.c -lm -ldvbapi */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>

#include <stdint.h>
#include <sys/time.h>
#include <math.h>
#include <locale.h>


#include <libdvbapi/dvbfe.h>
#include <linux/dvb/frontend.h>

#define FE_STATUS_PARAMS (DVBFE_INFO_LOCKSTATUS|DVBFE_INFO_SIGNAL_STRENGTH|DVBFE_INFO_BER|DVBFE_INFO_SNR|DVBFE_INFO_UNCORRECTED_BLOCKS)


static
int check_frontend (struct dvbfe_handle *fe, int human_readable, unsigned int count)
{
	struct dvbfe_info fe_info;
	unsigned int samples = 0;

	do {
		if (dvbfe_get_info(fe, FE_STATUS_PARAMS, &fe_info, DVBFE_INFO_QUERYTYPE_IMMEDIATE, 0) != FE_STATUS_PARAMS) {
	//		fprintf(stderr, "Problem retrieving frontend information: %m\n");
		}



		if (human_readable) {
                       printf ("status %c%c%c%c%c | signal %3u%% | snr %3u%% | ber %d | unc %d | ",
				fe_info.signal ? 'S' : ' ',
				fe_info.carrier ? 'C' : ' ',
				fe_info.viterbi ? 'V' : ' ',
				fe_info.sync ? 'Y' : ' ',
				fe_info.lock ? 'L' : ' ',
				(fe_info.signal_strength * 100) / 0xffff,
				(fe_info.snr * 100) / 0xffff,
				fe_info.ber,
				fe_info.ucblocks);
		} else {
			printf ("status %c%c%c%c%c | signal %04x | snr %04x | ber %08x | unc %08x | ",
				fe_info.signal ? 'S' : ' ',
				fe_info.carrier ? 'C' : ' ',
				fe_info.viterbi ? 'V' : ' ',
				fe_info.sync ? 'Y' : ' ',
				fe_info.lock ? 'L' : ' ',
				fe_info.signal_strength,
				fe_info.snr,
				fe_info.ber,
				fe_info.ucblocks);
		}

		if (fe_info.lock)
			printf("FE_HAS_LOCK");

		printf("\n");
		fflush(stdout);
		usleep(1000000);
		samples++;
	} while ((!count) || (count-samples));

	return 0;
}

void rotorCommand(struct dvbfe_handle *fe, int cmd, int n1, int n2, int n3 ) {
	struct dvb_diseqc_master_cmd cmds[] = {
		{ { 0xe0, 0x31, 0x60, 0x00, 0x00, 0x00 }, 3 },  //0 Stop Positioner movement
		{ { 0xe0, 0x31, 0x63, 0x00, 0x00, 0x00 }, 3 },  //1 Disable Limits
		{ { 0xe0, 0x31, 0x66, 0x00, 0x00, 0x00 }, 3 },  //2 Set East Limit
		{ { 0xe0, 0x31, 0x67, 0x00, 0x00, 0x00 }, 3 },  //3 Set West Limit
		{ { 0xe0, 0x31, 0x68, 0x00, 0x00, 0x00 }, 4 },  //4 Drive Motor East continously
		{ { 0xe0, 0x31, 0x68,256-n1,0x00, 0x00 }, 4 },  //5 Drive Motor East nn steps
		{ { 0xe0, 0x31, 0x69,256-n1,0x00, 0x00 }, 4 },  //6 Drive Motor West nn steps
		{ { 0xe0, 0x31, 0x69, 0x00, 0x00, 0x00 }, 4 },  //7 Drive Motor West continously
		{ { 0xe0, 0x31, 0x6a, n1, 0x00, 0x00 }, 4 },  //8 Store nn
		{ { 0xe0, 0x31, 0x6b, n1, 0x00, 0x00 }, 4 },   //9 Goto nn
		{ { 0xe0, 0x31, 0x6f, n1, n2, n3 }, 4}, //10 Recalculate Position
		{ { 0xe0, 0x31, 0x6a, 0x00, 0x00, 0x00 }, 4 },  //11 Enable Limits
		{ { 0xe0, 0x31, 0x6e, n1, n2, 0x00 }, 5 },   //12 Gotoxx
		{ { 0xe0, 0x10, 0x38, 0xF4, 0x00, 0x00 }, 4 }    //13 User
	};

	dvbfe_do_diseqc_command(fe, cmds[cmd].msg, cmds[cmd].msg_len);
	printf("n1, n2: %x, %x\n", n1, n2);

}
void gotoX(struct dvbfe_handle *fe, double azimuth ) {
	static const int DecimalLookup[10] = {
		 0x00, 0x02, 0x03, 0x05, 0x06, 0x08, 0x0A, 0x0B, 0x0D, 0x0E };
	double USALS=0.0;
	int CMD1=0x00, CMD2=0x00;

	if ( azimuth>0.0 )
		CMD1 = 0xE0;    // East
	else
		CMD1 = 0xD0;      // West

	USALS = fabs( azimuth );

	while (USALS > 16) {
		CMD1++;
		USALS-= 16;
	}
	while (USALS >= 1.0) {
		CMD2+=0x10;
		USALS--;
	}
	USALS*= 10.0;
	int rd = (int)USALS; //tenths

	USALS-= rd;
	if ( USALS>0.5 )
		++rd; //rounding

	CMD2+= DecimalLookup[rd];

	rotorCommand( fe, 12, CMD1, CMD2, 0 );
}

#define TO_RADS (M_PI / 180.0)
#define TO_DEC (180.0 / M_PI)

double getAzimuth( double angle, double latitude, double longitude ) {
	double P, Ue, Us, az, x, el, Azimuth;

	P = latitude*TO_RADS;           // Earth Station Latitude
	Ue = longitude*TO_RADS;           // Earth Station Longitude
	Us = angle*TO_RADS;          // Satellite Longitude

	az = M_PI+atan( tan( Us-Ue )/sin( P ) );
	x = acos( cos(Us-Ue)*cos(P) );
	el = atan( ( cos( x )-0.1513 )/sin( x ) );
	Azimuth = atan( ( -cos(el)*sin(az) )/( sin(el)*cos(P)-cos(el)*sin(P)*cos(az)) )* TO_DEC;

	return Azimuth;
}

void usage(char *exec_name) {
	printf("Usage: %s [opts] 23.5e 12525 v 27500\n", exec_name);
	printf("\t-a number : use given adapter (default 0)\n");
	printf("\t-f number : use given frontend (default 0)\n");
	printf("\t-c number : samples to take (default 0 = infinite)\n");
	printf("\t-H        : human readable output\n");
	printf("\t-l        : latitude  of position on earth \n");
	printf("\t-L        : longtitude of postion on earth \n");
}

int main(int argc, char *argv[])
{
	unsigned int adapter = 0, frontend = 0, count = 0, samples=0;
	int human_readable = 0;
	int opt;
	double angle,azimuth;
	char eastwest,pol;
	int freq, sr;
	int ifreq, hiband;

	int result;
	struct dvbfe_handle *fe;
	struct dvbfe_info fe_info;
	struct dvbfe_parameters fe_param;
	char *fe_type = "UNKNOWN";
	double latitude = 49.671093;
	double longitude = 14.156755;
	int i;
	char *ch;

	//setlocale (LC_ALL, "");

       while ((opt = getopt(argc, argv, "Ha:f:c:l:L:")) != -1) {
		switch (opt)
		{
		default:
			usage(argv[0]);
			exit(1);
			break;
		case 'a':
			adapter = strtoul(optarg, NULL, 0);
			break;
		case 'c':
			count = strtoul(optarg, NULL, 0);
			break;
		case 'f':
			frontend = strtoul(optarg, NULL, 0);
			break;
		case 'H':
			human_readable = 1;
			break;
		case 'l':
			latitude = strtod(optarg, NULL);
			break;
		case 'L':
			longitude = strtod(optarg, NULL);
			break;
		}
	}
	if (optind > argc-4) {
		usage(argv[0]);
		exit(1);
	}

	//check return valuas end booth state of options(west/east, vertical/horizontal)
	ch = malloc(strlen(argv[optind])+1);
	sscanf(argv[optind++], "%s", ch);
	eastwest = ch[strlen(ch)-1];;
	angle = atof(ch);
	free(ch);

/*	if (sscanf(argv[optind++], "%c", ch) < 2) {
		fprintf(stderr, "position %s not recognised\n", argv[--optind]);
		printf("angle %lf %c\n", angle, eastwest);
		exit(1);
	}*/

	printf("angle %lf%c\n", angle, eastwest);

	if (eastwest == 'w' || eastwest == 'W')
		angle = -angle;

	freq = strtol(argv[optind++], NULL, 0);
	if (argv[optind][0] == 'H' || argv[optind][0] == 'h') {
		pol=1;
	} else {
		pol=0;
	}
	sr = strtol(argv[++optind], NULL, 0);


	printf("Tuning to %d, %c, %d\n", freq, pol?'H':'V', sr);


	hiband = 0;
	if (freq > 11600)
		hiband = 1;
	ifreq = freq - (hiband?10600:9750);

	printf("IF: %d\n",ifreq);

	fe = dvbfe_open(adapter, frontend, 0);
	if (fe == NULL) {
		perror("opening frontend failed");
		return 0;
	}

	dvbfe_get_info(fe, 0, &fe_info, DVBFE_INFO_QUERYTYPE_IMMEDIATE, 0);
	switch(fe_info.type) {
	case DVBFE_TYPE_DVBS:
		fe_type = "DVBS";
		break;
	case DVBFE_TYPE_DVBC:
		fe_type = "DVBC";
		break;
	case DVBFE_TYPE_DVBT:
		fe_type = "DVBT";
		break;
	case DVBFE_TYPE_ATSC:
		fe_type = "ATSC";
		break;
	}
	printf("FE: %s (%s)\n", fe_info.name, fe_type);

	memset(&fe_param, 0, sizeof(fe_param));
	fe_param.frequency = ifreq*1000;
	fe_param.inversion = DVBFE_INVERSION_AUTO;
	fe_param.u.dvbs.symbol_rate = sr*1000;
	fe_param.u.dvbs.fec_inner = DVBFE_FEC_AUTO;

	azimuth = getAzimuth(angle, latitude, longitude);
	printf("Azimuth is %lf\n", azimuth);

	//dvbfe_set_voltage(fe, DVBFE_SEC_VOLTAGE_18);
	dvbfe_set_voltage(fe, pol?DVBFE_SEC_VOLTAGE_18:DVBFE_SEC_VOLTAGE_13);
	dvbfe_set_22k_tone(fe, DVBFE_SEC_TONE_OFF);
	usleep(500000);
	for (i=0; i<2; i++) {
		printf("Issuing rotation command.\n");
		gotoX(fe, azimuth);
		sleep(1);
	}

	sleep(1);

//	uint8_t diseqc_cmd[] = 
//	{ 0xe0, 0x10, 0x38, 0xf0, 0x00, 0x00};
//	diseqc_cmd[3] = 0xf0 | (((0 * 4) & 0x0f) | (hiband ? 1 : 0) | (pol ? 2 : 0));
//	dvbfe_do_diseqc_command(fe, diseqc_cmd, 4);

	dvbfe_set(fe, &fe_param, 0);
	dvbfe_set_voltage(fe, pol?DVBFE_SEC_VOLTAGE_18:DVBFE_SEC_VOLTAGE_13);
	dvbfe_set_22k_tone(fe, hiband?DVBFE_SEC_TONE_ON:DVBFE_SEC_TONE_OFF);



	check_frontend (fe, human_readable, count);
	
	dvbfe_close(fe);


	return 0;
}
