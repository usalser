# Makefile for usalser
binaries = usalser
prefix		= /usr/local
bindir		= $(prefix)/bin

inst_bin = $(binaries)

LDLIBS   += -ldvbapi -lm

.PHONY: all

all: $(binaries)

clean:
	rm -f usalser

install:
	install -d $(bindir)/
	install ./usalser $(bindir)/usalser

uninstall:
	rm $(bindir)/usalser


